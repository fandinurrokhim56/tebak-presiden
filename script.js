// Array data pertanyaan
const questions = [
    { question: "", correctAnswer: "Joko Widodo",              options: ['Soekarno', 'Soeharto', 'B.J. Habibie', 'Abdul Rahman Wahid', 'Megawati Soekarno Putri', 'Susilo Bambang Yudhoyono', 'Joko Widodo'], president: "Joko Widodo", hint: "Presiden Ketujuh"},
    { question: "", correctAnswer: "B.J. Habibie",             options: ['Soekarno', 'Soeharto', 'B.J. Habibie', 'Abdul Rahman Wahid', 'Megawati Soekarno Putri', 'Susilo Bambang Yudhoyono', 'Joko Widodo'], president: "B.J. Habibie", hint: "Presiden Ketiga"},
    { question: "", correctAnswer: "Soeharto",                 options: ['Soekarno', 'Soeharto', 'B.J. Habibie', 'Abdul Rahman Wahid', 'Megawati Soekarno Putri', 'Susilo Bambang Yudhoyono', 'Joko Widodo'], president: "Soeharto", hint: "Presiden Kedua"},
    { question: "", correctAnswer: "Soekarno",                 options: ['Soekarno', 'Soeharto', 'B.J. Habibie', 'Abdul Rahman Wahid', 'Megawati Soekarno Putri', 'Susilo Bambang Yudhoyono', 'Joko Widodo'], president: "Soekarno", hint: "Presiden Pertama"},
    { question: "", correctAnswer: "Abdul Rahman Wahid",       options: ['Soekarno', 'Soeharto', 'B.J. Habibie', 'Abdul Rahman Wahid', 'Megawati Soekarno Putri', 'Susilo Bambang Yudhoyono', 'Joko Widodo'], president: "Abdul Rahman Wahid", hint: "Presiden Keempat"},
    { question: "", correctAnswer: "Megawati Soekarno Putri",  options: ['Soekarno', 'Soeharto', 'B.J. Habibie', 'Abdul Rahman Wahid', 'Megawati Soekarno Putri', 'Susilo Bambang Yudhoyono', 'Joko Widodo'], president: "Megawati Soekarno Putri", hint: "Presiden Kelima"},
    { question: "", correctAnswer: "Susilo Bambang Yudhoyono", options: ['Soekarno', 'Soeharto', 'B.J. Habibie', 'Abdul Rahman Wahid', 'Megawati Soekarno Putri', 'Susilo Bambang Yudhoyono', 'Joko Widodo'], president: "Susilo Bambang Yudhoyono", hint: "Presiden Keenam"},
];

// Biodata President
const presidentsData = {
    Soekarno: {
        Nama: "Soekarno",
        Tanggal_Lahir: "6 Juni 1901",
        Tempat_Lahir: "Blitar, Jawa Timur",
        Pendidikan: "Technische Hoogeschool te Bandoeng",
        Partai: "PNI",
        Masa_Jabatan: "1945 - 1967",
        Deskripsi: "Soekarno adalah presiden pertama Republik Indonesia.",
        foto: "images/soekarno.jpg"
    },
    Soeharto: {
        Nama: "Soeharto",
        Tanggal_Lahir: "8 Juni 1921",
        Tempat_Lahir: "Yogyakarta, Hindia Belanda",
        Pendidikan: "Koninklijke Militaire Academie",
        Partai: "Golkar",
        Masa_Jabatan: "1967 - 1998",
        Deskripsi: "Soeharto adalah presiden kedua Republik Indonesia.",
        foto: "images/soeharto.jpg"
    },
    "B.J. Habibie": {
        Nama: "B.J. Habibie",
        Tanggal_Lahir: "25 Juni 1936",
        Tempat_Lahir: "Parepare, Sulawesi Selatan",
        Pendidikan: "RWTH Aachen University",
        Partai: "Golkar",
        Masa_Jabatan: "1998 - 1999",
        Deskripsi: "B.J. Habibie adalah presiden ketiga Republik Indonesia.",
        foto: "images/habibie.jpg"
    },
    "Abdul Rahman Wahid": {
        Nama: "Abdul Rahman Wahid",
        Tanggal_Lahir: "7 Agustus 1940",
        Tempat_Lahir: "Jombang, Jawa Timur",
        Pendidikan: "Al-Azhar University, Kairo",
        Partai: "PKB",
        Masa_Jabatan: "1999 - 2001",
        Deskripsi: "Abdul Rahman Wahid adalah presiden keempat Republik Indonesia.",
        foto: "images/abdulrahmanwahid.jpg"
    },
    "Megawati Soekarno Putri": {
        Nama: "Megawati Soekarno Putri",
        Tanggal_Lahir: "23 Januari 1947",
        Tempat_Lahir: "Yogyakarta, Indonesia",
        Pendidikan: "Universitas Indonesia",
        Partai: "PDI-P",
        Masa_Jabatan: "2001 - 2004",
        Deskripsi: "Megawati Soekarno Putri adalah presiden kelima Republik Indonesia.",
        foto: "images/megawati.jpg"
    },
    "Susilo Bambang Yudhoyono": {
        Nama: "Susilo Bambang Yudhoyono",
        Tanggal_Lahir: "9 September 1949",
        Tempat_Lahir: "Pacitan, Jawa Timur",
        Pendidikan: "Akademi Militer Nasional",
        Partai: "Partai Demokrat",
        Masa_Jabatan: "2004 - 2014",
        Deskripsi: "Susilo Bambang Yudhoyono adalah presiden keenam Republik Indonesia.",
        foto: "images/sby.jpg"
    },
    "Joko Widodo": {
        Nama: "Joko Widodo",
        Tanggal_Lahir: "21 Juni 1961",
        Tempat_Lahir: "Surakarta, Jawa Tengah",
        Pendidikan: "Universitas Gadjah Mada",
        Partai: "PDIP",
        Masa_Jabatan: "2014 - Sekarang",
        Deskripsi: "Joko Widodo adalah presiden ketujuh Republik Indonesia.",
        foto: "images/jokowi.jpg"
    }
};

let currentPresident;
let currentQuestion = 0;
let score = 0;

const questionElement    = document.getElementById('question');
const answerInputElement = document.getElementById('answer-input');
const resultElement      = document.getElementById('result');
const scoreElement       = document.getElementById('score');
const optionsElement     = document.getElementById('options');
const controlButton      =  document.getElementById('control-buttons');
const displayFull        = document.getElementById('display-full');
const backgroundMusic    = document.getElementById('backgroundMusic');

// Fungsi untuk mengacak urutan array
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}
  
questions.forEach((question) => {
    question.options = shuffleArray(question.options);
});

    
function displayHint() {
    const currentHint         = questions[currentQuestion].hint;
    const hintModalBody       = document.getElementById('hint-modal-body');
    hintModalBody.textContent = currentHint;
}  

function displayQuestion() {
    currentPresident = questions[currentQuestion].president;
    const currentOptions = questions[currentQuestion].options;
    questionElement.textContent = questions[currentQuestion].question;

    // Tampilkan input dan button jawab
    jawaban.classList.remove('hidden');
    pertanyaan.classList.remove('hidden');

    backgroundMusic.play();

    // Hapus pilihan sebelumnya
    optionsElement.innerHTML = '';

    // Button untuk tiap pilihan
    for (let i = 0; i < currentOptions.length; i++) {
        const optionButton = document.createElement('button');
        optionButton.textContent = currentOptions[i];
        optionButton.classList.add('btn', 'btn-outline-dark', 'me-3', 'mt-3', 'answer-button');
        optionButton.onclick = function () {
            selectOption(currentOptions[i]);
        };
        optionButton.style.fontWeight = 'normal';

        optionsElement.appendChild(optionButton);
    }

    const presidentImg = document.getElementById('foto-presiden');
    const fotoPresiden = presidentsData[currentPresident].foto;
    presidentImg.src = fotoPresiden;

    const hintButton = document.getElementById('hint-button');
    hintButton.onclick = function() {
        displayHint();
    };

    resetDisplayFullSize();
}


function selectOption(option) {
    answerInputElement.value = option;

    const optionButtons = optionsElement.getElementsByTagName('button');
    for (let i = 0; i < optionButtons.length; i++) {
        optionButtons[i].classList.remove('selected', 'btn-dark');
        optionButtons[i].classList.add('btn-outline-dark');
        optionButtons[i].style.fontWeight = 'normal';
    }

    // tambah style btn dipilih
    event.target.classList.add('selected', 'btn-dark', 'fw-bold');
}


// Fungsi untuk menampilkan hasil jawaban
function showResult(isCorrect) {
    resultElement.textContent = isCorrect ? 'Jawaban Anda Benar!' : 'Jawaban Anda Salah!';
    resultElement.classList.remove('bg-danger', 'bg-success');
    resultElement.classList.add(isCorrect ? 'bg-success' : 'bg-danger');
    resultElement.classList.toggle('text-light', isCorrect);
  
    backgroundMusic.pause();
  
    const audio = new Audio();
    if (isCorrect) {
      audio.src = 'Asset game/jawaban benar.wav';
    } else {
      audio.src = 'Asset game/jawaban salah.wav';
    }
    audio.play();
  }
  

// Function to check the answer
function checkAnswer() {
    const userAnswer = answerInputElement.value.toLowerCase();
    const correctAnswer = questions[currentQuestion].correctAnswer.toLowerCase();

    if (userAnswer === correctAnswer) {
        showResult(true);
        score++;
    } else {
        showResult(false);
    }

    displayBiodata();
    answerInputElement.disabled = true;
    playButtonSound(); 
}


// Function to display biodata
function displayBiodata() {
    const currentPresident = questions[currentQuestion].president;
    const biodata = presidentsData[currentPresident];
  
    questionElement.textContent = "";
    optionsElement.innerHTML = "";
  
    const biodataList = document.getElementById('biodata-list');
    biodataList.innerHTML = "";
  
    // Set the image source
    const presidentImg = document.getElementById('president-img');
    presidentImg.src = presidentsData[currentPresident].foto;
  
    // Properti yang ingin ditampilkan
    const displayedProperties = ['Nama', 'Tanggal_Lahir', 'Tempat_Lahir', 'Pendidikan', 'Partai', 'Masa_Jabatan', 'Deskripsi'];
  
    for (const property in biodata) {
      if (displayedProperties.includes(property)) {
        const listItem = document.createElement('li');
        listItem.textContent = biodata[property];
        listItem.classList.add('list-group-item', 'border-0', 'text-start', 'biodata');
        biodataList.appendChild(listItem);
      }
    }
  
    const biodataContainer = document.getElementById('biodata-container');
    biodataContainer.classList.remove('hidden');
    pertanyaan.classList.add('hidden');
    jawaban.classList.add('hidden');
  
    setDisplayFullSize();
}
  
function hideBiodata() {
    const biodataContainer = document.getElementById('biodata-container');
    biodataContainer.classList.add('hidden');
}

// Fungsi untuk mengubah ukuran elemen menjadi col-md-12
function setDisplayFullSize() {
  displayFull.classList.remove('col-md-6');
  displayFull.classList.add('col-md-12');
}

// Fungsi untuk mengembalikan ukuran elemen menjadi col-md-6
function resetDisplayFullSize() {
  displayFull.classList.remove('col-md-12');
  displayFull.classList.add('col-md-6');
}


function previousQuestion() {
    currentQuestion--;

    if (currentQuestion >= 0) {
        displayQuestion();
        resultElement.textContent = "";
        answerInputElement.disabled = false;
        hideBiodata();
        playButtonSound(); // Memainkan suara efek game saat tombol diklik
    } else {
        currentQuestion = 0;
    }
}

function nextQuestion() {
    currentQuestion++;

    if (currentQuestion < questions.length) {
        displayQuestion();
        resultElement.textContent = "";
        answerInputElement.disabled = false;
        hideBiodata();
        playButtonSound(); // Memainkan suara efek game saat tombol diklik
    } else {
        endGame();
        hideBiodata();
    }
}


// Function to end the game and display final score
function endGame() {
    questionElement.textContent = "Permainan Selesai!";
    resultElement.textContent = "Skor Akhir: " + score + " dari " + questions.length;
    answerInputElement.style.display = "none";
    scoreElement.textContent = "";

    // Mengubah properti display tombol-tombol
    homeButton.style.display = "inline-block";
    retryButton.style.display = "inline-block";

    optionsElement.style.display = "none";

    const audio = new Audio();

    audio.src = 'Asset game/skor akhir.wav';
    audio.play();
}


displayQuestion();
