// Mendapatkan referensi elemen-elemen yang diperlukan
const audioElement = document.getElementById('backgroundMusic');
const toggleButton = document.getElementById('audioControls').querySelector('button');
const volumeSlider = document.getElementById('volumeSlider');
const video = document.getElementById('backgroundVideo');

// Fungsi untuk mengubah status pemutaran audio
function toggleAudio() {
  if (audioElement.paused) {
    audioElement.play();
    toggleButton.textContent = 'Pause Audio';
  } else {
    audioElement.pause();
    toggleButton.textContent = 'Play Audio';
  }
}

// Fungsi untuk mengubah volume audio
function changeVolume(volume) {
  audioElement.volume = volume;
}

// Inisialisasi nilai volume awal
volumeSlider.value = audioElement.volume;

audioElement.addEventListener('play', function() {
    toggleButton.innerHTML = '<i class="fas fa-volume-up"></i>';
});
  
  audioElement.addEventListener('pause', function() {
    toggleButton.innerHTML = '<i class="fas fa-volume-mute"></i>';
});

// Function loop vidio
video.addEventListener('ended', function() {
  // Setelah video selesai, tunggu 3 detik dan putar ulang
  setTimeout(function() {
    video.currentTime = 0; // Mengatur waktu video ke awal
    video.play(); // Memulai pemutaran ulang video
  }, 3000);
});




// ======== SUARA EFEK GAME ==============
// Function play button sound
function playButtonSound() {
  const buttonSound = document.getElementById('buttonSound');
  buttonSound.currentTime = 0; 
  buttonSound.play(); 
}

